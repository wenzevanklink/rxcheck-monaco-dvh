===============================
RxCheck Monaco DVH Backend
===============================

.. image:: https://badge.fury.io/py/rxcheck-monaco-dvh.png
    :target: http://badge.fury.io/py/rxcheck-monaco-dvh
    
.. image:: https://travis-ci.org/tohccmedphys/rxcheck-monaco-dvh.png?branch=master
        :target: https://travis-ci.org/tohccmedphys/rxcheck-monaco-dvh

.. image:: https://pypip.in/d/rxcheck-monaco-dvh/badge.png
        :target: https://pypi.python.org/pypi/rxcheck-monaco-dvh


An RxCheck backend for importing DVH data exported from Monaco 5

* Free software: BSD license
* Documentation: http://rxcheck-monaco-dvh.rtfd.org.

Features
--------

* TODO