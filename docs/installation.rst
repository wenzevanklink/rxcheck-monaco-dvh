============
Installation
============

At the command line::

    $ easy_install rxcheck-monaco-dvh

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv rxcheck-monaco-dvh
    $ pip install rxcheck-monaco-dvh