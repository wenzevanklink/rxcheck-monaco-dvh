from rxcheck_base_importer import BaseImporter

import dvh
import numpy
import pandas
import os
import re

import settings

HEADER_RE = re.compile(
    """
    ^Patient\sID:\s(?P<patient_id>[~0-9a-zA-Z_+]+).+
    Plan\sName:\s(?P<plan_id>[~0-9a-zA-Z_+]+).+
    Resolution:\s(?P<resolution>[0-9.]+)\((?P<resolution_unit>[a-zA-Z]+)\).+
    Bin\sWidth:\s(?P<bin_width>[0-9.]+)\((?P<bin_width_unit>[a-zA-Z]+)\).+
    Dose\s\w+:\s(?P<dose_unit>[a-zA-Z]+)\s.+
    Volume\s\w+:\s(?P<volume_unit>[a-zA-Z%]+.?).*
    """,
    re.VERBOSE,
)


def absolute_file_paths(directory):
   for dirpath, _, filenames in os.walk(directory):
       for f in filenames:
           yield os.path.abspath(os.path.join(dirpath, f))


class MonacoDVHImporter(BaseImporter):

    NAME = "Monaco DVH"
    HANDLE = "monacodvh"

    def __init__(self, *args, **kwargs):

        self.import_dir = kwargs.pop("dvh_directory", settings.MONACO_DVH_DIRECTORY)

        super(MonacoDVHImporter, self).__init__(*args, **kwargs)


    def get_patients(self):

        paths = absolute_file_paths(self.import_dir)

        patients = {}
        for path in paths:
            try:
                f = open(path,"r")
                header_info = HEADER_RE.match(f.readline()).groupdict()
                patient_id = header_info["patient_id"]
                patient_id = self.clean_patient_id(patient_id)
                plan_id=header_info["plan_id"]
                plan = {
                    "name":plan_id,
                }

                if patient_id in patients:
                    patients[patient_id][plan_id] = plan
                else:
                    patients[patient_id] = {
                        plan_id: plan,
                    }
            except:
                pass

        return patients

    def get_patient_plan(self, patient_id, plan_id):
        path = self.find_dvh_path(patient_id, plan_id)
        if path:
            return self.get_plan_data(path)

    def clean_patient_id(self, patient_id):
        if "~" in patient_id:
            patient_id = patient_id.split("~")[-1]
        return patient_id

    def find_dvh_path(self, patient_id, plan_id):
        """return path of the DVH file corresponding to the input patient_id & plan_id
        This just naively opens every single file in the MONACO_DVH_DIRECTORY and tries
        to match the header data looking for the correct patient/plan.
        """

        paths = absolute_file_paths(settings.MONACO_DVH_DIRECTORY)

        for path in paths:
            try:
                with open(path, "r") as f:
                    try:
                        header_info = HEADER_RE.match(f.readline()).groupdict()
                        pid = self.clean_patient_id(header_info["patient_id"])
                        if (pid, header_info["plan_id"]) == (patient_id, plan_id):
                            return path
                    except AttributeError:
                        # not a valid Monaco DVH file
                        pass
            except IOError: # pragma: nocover
                # file got moved or deleted or locked?
                # should rarely ever get hit since we already
                # know the file exists
                pass

        raise ValueError("Unable to find valid DVH file for plan {0} for patient {1}".format(patient_id, plan_id))

    def get_plan_data(self, path):
        """return dvh data for input file path."""

        with open(path, "r") as f:

            header_data = HEADER_RE.match(f.readline()).groupdict()
            if header_data["dose_unit"] == "cGy":
                multiplier = 1.
            elif header_data["dose_unit"] == "Gy":
                multiplier = 100.
            else:
                raise ValueError("Unknown dose unit")

            if "cm" not in header_data["volume_unit"]:
                raise ValueError("Invalid volume mode {0}. Must be absolute volume in cc's".format(header_data["volume_unit"]))

            f.seek(0)

            data_frame = pandas.read_csv(f, sep=" "*20, skiprows=3, skip_footer=3, names=["struct","dose","volume"])
            data_frame.dose *= multiplier

            bin_width = float(header_data["bin_width"]) * multiplier

            plan = {
                "name": header_data["plan_id"],
                "id": header_data["plan_id"],
                "structures": self.structures_from_data_frame(data_frame, bin_width)
            }


        return plan

    def structures_from_data_frame(self, data_frame, bin_width):

        grouped = data_frame.groupby("struct")
        dvh_volumes = grouped.volume
        dvh_doses = grouped.dose

        structures = []
        for name in grouped.groups.keys():

            volumes = numpy.array(dvh_volumes.get_group(name), dtype=numpy.float)

            if not dvh.monotonic_decreasing(volumes):
                raise ValueError("DVH must be exported as a cumulative dvh with absolute volume")

            struct_volume = volumes.max()

            doses =  numpy.array(dvh_doses.get_group(name))
            struct_dvh =  dvh.DVH(doses, volumes)

            structures.append({
                "name":name,
                "volume":struct_volume,
                "dvh":{
                    "doses":struct_dvh.doses.tolist(),
                    "volumes":struct_dvh.cum_volumes.tolist(),
                },
            })

        return structures
