import os, sys

try:
    from django.conf import settings
    settings.DEBUG
except:
    settings = {}

base =  os.path.dirname(__file__)
tests = os.path.join(base, "..", "tests")
test_dvhs = os.path.join(tests, "dvhs")

MONACO_DVH_DIRECTORY = getattr(settings, "MONACO_DVH_DIRECTORY", test_dvhs)

GRAY = "Gy"
CENTIGRAY = "cGy"

