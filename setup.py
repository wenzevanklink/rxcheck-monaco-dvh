#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')
import glob
setup(
    name='rxcheck-monaco-dvh',
    version='0.1.0',
    description='An RxCheck backend for importing DVH data exported from Monaco 5',
    long_description=readme + '\n\n' + history,
    author='Randle Taylor',
    author_email='randle.taylor@gmail.com',
    url='https://bitbucket.org/tohccmedphys'
        '/rxcheck-monaco-dvh',
    packages=[
        'monacodvh',
    ],
    package_dir={'monacodvh': 'monacodvh'},
    data_files=[ ("tests/dvhs",  glob.glob('tests/dvhs/*')),],
    include_package_data=True,
    install_requires=[
        'dvh',
        'rxcheck-base-importer',
    ],
    license="BSD",
    zip_safe=False,
    keywords='rxcheck-monaco-dvh',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    ],
    test_suite='tests',
)
