#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_monacodvh
----------------------------------

Tests for `monacodvh` module.
"""

import unittest

import monacodvh.monacodvh as monacodvh

class TestMonacoDVH(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        self.all_patients = {
            "BVMAT09ABS": {
                "vmat": {
                    "name":"vmat",
                },
            },

            "BVMAT09": {
                "vmat": {
                    "name":"vmat",
                },
            },

            "BVMAT11": {
                "vmat1": {
                    "name":"vmat1",
                },
                "vmat2": {
                    "name":"vmat2",
                },
                "vmat3": {
                    "name":"vmat3",
                },
            },

            "PROSTATE": {
                "ProstVMAT": {
                    "name":"ProstVMAT",
                },
            },

            "12345678": {
                "Course1+Course2": {
                    "name":"Course1+Course2",
                },

                "bad_unit": {
                    "name":"bad_unit",
                },

                "bad_vol_unit": {
                    "name":"bad_vol_unit",
                },
            },
        }


        self.patient_id = "BVMAT11"
        self.patient_plan_id = "vmat1"
        self.structures = {
            "Axilla II":{"volume":30.918},
            "Axilla III":{"volume":7.521},
            "Boost PTV":{"volume":31.596},
            "Breast PTV":{"volume":707.256},
            "Contol-breast":{"volume":878.664},
            "Cord":{"volume":None},
            "CT0":{"volume":None},
            "esophagus":{"volume":None},
            "external(Unsp.Tiss.)":{"volume":None},
            "Heart": {"volume":None},
            "ICN": {"volume":None},
            "IMC":{"volume":None},
            "LCA-Cor":{"volume":None},
            "LCircumflex":{"volume":None},
            "Liver":{"volume":None},
            "Lt lung":{"volume":None},
            "LVentricle":{"volume":None},
            "Plex Brach":{"volume":None},
            "PTV":{"volume":1008.915},
            "RCoronary":{"volume":None},
            "Rt lung": {"volume":None},
            "RVentricle":{"volume":None},
            "scar":{"volume":None},
            "SCN":{"volume":None},
            "seroma":{"volume":None},
            "Skin1":{"volume":None},
            "thyroid": {"volume":None},
        }

        self.importer = monacodvh.MonacoDVHImporter()

    def test_patient_ids(self):
        patients =  self.importer.get_patients().keys()
        self.assertSetEqual(set(patients), set(self.all_patients.keys()))

    def test_patient_plans(self):
        patients =  self.importer.get_patients()
        self.assertDictEqual(patients, self.all_patients)

    def test_structs(self):
        plan = self.importer.get_patient_plan(self.patient_id, self.patient_plan_id)
        self.assertSetEqual(set(plan["structures"].keys()),set(self.structures.keys()))

    def test_volumes(self):
        plan = self.importer.get_patient_plan(self.patient_id, self.patient_plan_id)
        for name, props in self.structures.iteritems():
            if props["volume"] is not None:
                self.assertAlmostEqual(plan["structures"][name]["volume"], props["volume"])

    def test_Gy_input(self):
        plan = self.importer.get_patient_plan(self.patient_id, "vmat3")
        dvh_data = plan["structures"]["Axilla II"]["dvh"]
        self.assertAlmostEqual(dvh_data["doses"][1], 49.25*100)

    def test_cGy_input(self):
        plan = self.importer.get_patient_plan("12345678", "Course1+Course2")
        dvh_data = plan["structures"]["bladder"]["dvh"]
        self.assertAlmostEqual(dvh_data["doses"][1], 395)

    def test_bad_dose_unit(self):
        with self.assertRaises(ValueError):
            plan = self.importer.get_patient_plan("12345678", "bad_unit")

    def test_bad_vol_unit(self):
        with self.assertRaises(ValueError):
            plan = self.importer.get_patient_plan("12345678", "bad_vol_unit")

    def test_number_bins_correct(self):
        plan = self.importer.get_patient_plan(self.patient_id, "vmat3")
        dvh_data = plan["structures"]["Axilla II"]["dvh"]
        self.assertEqual(len(dvh_data["doses"]), len(dvh_data["volumes"]))

    def test_missing_file(self):
        with self.assertRaises(ValueError):
            self.importer.find_dvh_path("foo", "bar")


    def test_load_differential(self):
        with self.assertRaises(ValueError):
            self.importer.get_patient_plan("BVMAT09ABS", "vmat")

if __name__ == '__main__':
    unittest.main()
